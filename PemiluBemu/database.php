<?php
	function connect_database() 
	{
		//konfigurasi database
		$db_user = "bpmu";
		$db_password = "bpmu";
		$db_name = "pemilu";
		$db_host = "localhost";
		
		//akan selalu dilakukan, lebih baik jika ditulis dalam sebuah fungsi tersendiri
		$koneksi = mysqli_connect($db_host, $db_user, $db_password, $db_name);
		return $koneksi;
	}


	function cekexist ($nim ,$nama)
	{
		$koneksi = connect_database();

		$nim = mysqli_real_escape_string($koneksi, $nim);
		$nama = mysqli_real_escape_string($koneksi, $nama);
		

		$sql = "Select * from User where nim= '$nim' and nama='$nama'";
		$result = mysqli_query($koneksi, $sql);

		mysqli_close($koneksi);

		if(mysqli_num_rows($result)!= 1)
		{
			return false;
		}
		else
		{
			return true;
		}

	}

	function cekdone($nim)
	{
		$koneksi = connect_database();

		$nim = mysqli_real_escape_string($koneksi, $nim);

		

		$sql = "Select done from User where nim= '$nim' ";
		$result = mysqli_query($koneksi, $sql);

		$row = mysqli_fetch_assoc($result);
		$return = $row["done"];

		

		mysqli_close($koneksi);

		return $return;

	}

	function update_post($nim, $vote, $alasan, $calon) 
	{
		$koneksi = connect_database();

		//escape input
		$alasan = mysqli_real_escape_string($koneksi, $alasan);
		$calon = mysqli_real_escape_string($koneksi, $calon);

		
		$sql = "UPDATE User Set vote='$vote', alasan='$alasan', calon='$calon', done=1 where nim='$nim' ";
		
		$check=mysqli_query($koneksi,$sql);
		if($check)
		{
			echo "success";
		}
		else
		{
			 echo mysqli_error($koneksi);# code...
		}
		
		mysqli_close($koneksi);
	}

	function see_voter()
	{

		$koneksi = connect_database();

		$sql = "SELECT count(nim) FROM `user` WHERE done = 1";

		$result = mysqli_query($koneksi,$sql);

		$row = mysqli_fetch_assoc($result);
		$x = $row["count(nim)"];

		mysqli_close($koneksi);

		return $x;
	}

	function see_yes()
	{

		$koneksi = connect_database();

		$sql = "SELECT count(nim) FROM `user` WHERE vote = 1";

		$result = mysqli_query($koneksi,$sql);

		$row = mysqli_fetch_assoc($result);
		$x = $row["count(nim)"];

		mysqli_close($koneksi);

		return $x;
	}

	function see_no()
	{

		$koneksi = connect_database();

		$sql = "SELECT count(nim) FROM `user` WHERE vote = -1";

		$result = mysqli_query($koneksi,$sql);

		$row = mysqli_fetch_assoc($result);
		$x = $row["count(nim)"];

		mysqli_close($koneksi);

		return $x;
	}

	function see_golput()
	{

		$koneksi = connect_database();

		$sql = "SELECT count(nim) FROM `user` WHERE vote = 0";

		$result = mysqli_query($koneksi,$sql);

		$row = mysqli_fetch_assoc($result);
		$x = $row["count(nim)"];

		mysqli_close($koneksi);

		return $x;
	}

	function cektime()
	{
		if(date("H")>15 || date("H")<8)
		{
			header("Location: timeout.html");
		}
	}