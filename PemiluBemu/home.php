<?php session_start(); 
if(isset($_SESSION["nama"]))
{
?>
<!DOCTYPE html>
<html lang="en">

<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Login screen">
    <meta name="author" content="A.H.I.E">

    <title>Pemilu BEMU 2016</title>

    <link rel="icon" href="img/ref-icon.png" type="image/png" sizes="16x16">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

</head>

<body>
    <div class="container">
        <!-- Modal Setuju-->
        <div class="modal fade" id="modalSetuju" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <form method="post" action="voteyes.php">
                <div class="modal-content">
                <div class="modal-header" style="background-color:#5CB85C;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:#FFF;">Setuju</h4>
                </div>
                <div class="modal-body">
                    <p>Anda yakin untuk memilih Setuju ?</p>
                </div>
                <div class="modal-footer">
                    <input type="submit" name="login" class="btn btn-success" value="Ya">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Kembali</button>
                </div>
                </div>
            </form>
        </div>
        </div>

        <!-- Modal Tidak Setuju-->
        <div class="modal fade" id="modalTidakSetuju" role="dialog">
        <div class="modal-dialog">
            <!-- Modal content-->
            <form method="post" action="voteno.php">
                <div class="modal-content">
                <div class="modal-header" style="background-color:#D43F3A;">
                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                    <h4 class="modal-title" style="color:#FFF;">Tidak Setuju</h4>
                </div>
                <div class="modal-body">
                    <p>Karena anda memilih untuk tidak setuju maka kami meminta alasan kenapa anda memilih untuk tidak setuju. Kemudian berikan nama lengkap kandidat yang menurut anda lebih layak.</p>
                    <div class="form-group">
                        <label for="alasan">Berikan alasan anda kenapa anda memilih untuk tidak setuju : </label>
                        <textarea class="form-control" rows="5" id="alasan" name="alasan"></textarea>
                    </div>
                    <div class="form-group">
                        <label for="rekomendasi">Berikan nama lengkap kandidat rekomendasi anda : </label>
                        <input type="text" id="rekomendasi" name="calon" class="form-control input-sm"/>
                    </div>          
                </div>
                <div class="modal-footer">
                    <input type="submit" name="login" class="btn btn-danger" value="Kirim">
                    <button type="button" class="btn btn-default" data-dismiss="modal" >Kembali</button>
                </div>
                </div>
            </form>
        </div>
        </div>
    </div>

    <!-- Header -->
    <header id="top" class="header">
        <div class="text-vertical-center" style="color:#333;">
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <br>
            <h2>Selamat datang di</h2>
            <h1>PEMILU BEMU UKDW 2016</h1>
            <!--<h4>- Persatuan Indonesia</h4>
            <h3><strong>Misi : </strong></h3>
            <h4>- Ketuhanan Yang Maha Esa</h4>
            <h4>- Kemanusiaan yang Adil dan Beradab</h4>
            <h4>- Persatuan Indonesia</h4>
            <br>-->
        </div>
    </header>

    <!-- About -->
    <section id="about" class="about" style="background-color : rgb(222, 222, 222);">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-left">
                    <h2><strong>CALON PRESIDEN</strong></h2>
                    <p class="lead">Valdi Haris Adi Nugroho</p>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 text-right">
                    <h2><strong>CALON WAKIL PRESIDEN</strong></h2>
                    <p class="lead">Verdha Ayu Paramitha</p>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>
    

    <!-- About -->
    <section id="about" class="about">
        <div class="container">
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><strong>VISI</strong></h2>
                    <p class="lead">Mewujudkan organisasi kemahasiswaan menjadi wadah yang kondusif bagi mahasiswa untuk mengasah semua <em>softskill</em> yang dibutuhkan sesuai bidang minat.</p>
                </div>
            </div>
            <!-- /.row -->
            <div class="row">
                <div class="col-lg-12 text-center">
                    <h2><strong>MISI</strong></h2>
                    <p class="lead">Menjadikan organisasi kemahasiswaan sebagai wadah yang kondusif bagi mahasiswa untuk menyalurkan minat bakat.</p>
                    <p class="lead">Meningkatkan kesadaran mahasiswa akan pentingnya berorganisasi.</p>
                </div>
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Services -->
    <section id="services" class="services bg-primary" style="background-color:rgb(205, 203, 214);">
        <div class="container">
            <div class="row text-center">
                <div class="col-lg-10 col-lg-offset-1">
                    <h2 style="color:#333"><strong>PILIHAN ANDA</strong></h2>
                    <hr class="small">
                    <div class="row">
                        <div class="col-md-6 col-sm-6">
                            <div class="service-item">
                                <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalSetuju" style="background-color:transparent; border: none;">
                                    <span class="fa-stack fa-4x">
                                        <i class="fa fa-circle fa-stack-2x" style="color:#5CB85C"></i>
                                        <i class="glyphicon glyphicon-ok fa-stack-1x text-primary" style="color:#FFF"></i>
                                    </span>
                                </button>
                                <h4 style="color:#333;">
                                    <button type="button" class="btn btn-success btn-lg" data-toggle="modal" data-target="#modalSetuju"><strong>  SETUJU</strong></button>
                                </h4>
                            </div>
                        </div>
                        <div class="col-md-6 col-sm-6">
                            <div class="service-item">
                                <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalTidakSetuju" style="background-color:transparent; border: none;">
                                    <span class="fa-stack fa-4x">
                                        <i class="fa fa-circle fa-stack-2x" style="color:#D9534F"></i>
                                        <i class="glyphicon glyphicon-remove fa-stack-1x text-primary" style="color:#FFF"></i>
                                    </span>
                                </button>
                                <h4 style="color:#333;">
                                    <button type="button" class="btn btn-danger btn-lg" data-toggle="modal" data-target="#modalTidakSetuju"><strong>  TIDAK SETUJU</strong></button>
                                </h4>
                            </div>
                        </div>
                    </div>
                    <!-- /.row (nested) -->
                </div>
                <!-- /.col-lg-10 -->
            </div>
            <!-- /.row -->
        </div>
        <!-- /.container -->
    </section>

    <!-- Footer -->
    <footer>
        <div class="container">
            <div class="row">
                <div class="col-lg-10 col-lg-offset-1 text-center">
                    <h4>
                        <strong>Pemilu BEMU 2016</strong>
                    </h4>
                    <img src="img/ref-icon.png" style="width:100px; height:100px;">
                    <img src="img/icon-bpmu.png" style="width:100px; height:100px;">
                    <br><br>
                    <p class="text-muted">Copyright &copy; BPMU 2016</p>
                </div>
            </div>
        </div>
        <a id="to-top" href="#top" class="btn btn-dark btn-lg"><i class="fa fa-chevron-up fa-fw fa-1x"></i></a>
    </footer>

    <!-- jQuery -->
    <script src="js/jquery.js"></script>
    <script src="https://ajax.googleapis.com/ajax/libs/jquery/3.1.1/jquery.min.js"></script>

    <!-- Bootstrap Core JavaScript -->
    <script src="js/bootstrap.min.js"></script>

    <!-- Custom Theme JavaScript -->
    <script>
    // Closes the sidebar menu
    $("#menu-close").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    // Opens the sidebar menu
    $("#menu-toggle").click(function(e) {
        e.preventDefault();
        $("#sidebar-wrapper").toggleClass("active");
    });
    // Scrolls to the selected menu item on the page
    $(function() {
        $('a[href*=#]:not([href=#],[data-toggle],[data-target],[data-slide])').click(function() {
            if (location.pathname.replace(/^\//, '') == this.pathname.replace(/^\//, '') || location.hostname == this.hostname) {
                var target = $(this.hash);
                target = target.length ? target : $('[name=' + this.hash.slice(1) + ']');
                if (target.length) {
                    $('html,body').animate({
                        scrollTop: target.offset().top
                    }, 1000);
                    return false;
                }
            }
        });
    });
    //#to-top button appears after scrolling
    var fixed = false;
    $(document).scroll(function() {
        if ($(this).scrollTop() > 250) {
            if (!fixed) {
                fixed = true;
                // $('#to-top').css({position:'fixed', display:'block'});
                $('#to-top').show("slow", function() {
                    $('#to-top').css({
                        position: 'fixed',
                        display: 'block'
                    });
                });
            }
        } else {
            if (fixed) {
                fixed = false;
                $('#to-top').hide("slow", function() {
                    $('#to-top').css({
                        display: 'none'
                    });
                });
            }
        }
    });
    // Disable Google Maps scrolling
    // See http://stackoverflow.com/a/25904582/1607849
    // Disable scroll zooming and bind back the click event
    var onMapMouseleaveHandler = function(event) {
        var that = $(this);
        that.on('click', onMapClickHandler);
        that.off('mouseleave', onMapMouseleaveHandler);
        that.find('iframe').css("pointer-events", "none");
    }
    var onMapClickHandler = function(event) {
            var that = $(this);
            // Disable the click handler until the user leaves the map area
            that.off('click', onMapClickHandler);
            // Enable scrolling zoom
            that.find('iframe').css("pointer-events", "auto");
            // Handle the mouse leave event
            that.on('mouseleave', onMapMouseleaveHandler);
        }
        // Enable map zooming with mouse scroll when the user clicks the map
    $('.map').on('click', onMapClickHandler);
    </script>

</body>

</html>
<?php } 
else header("Location: index.php");
?>