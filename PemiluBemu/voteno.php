<?php

	session_start();

	$nim = $_SESSION["nim"];

	require_once("database.php");

	$alasan = $_POST["alasan"];
	$calon = $_POST["calon"];

	if($alasan != "" && $calon!="")
	{
		update_post($nim,-1,$alasan,$calon);

	}
	else
	{
		update_post($nim,0,$alasan,$calon);
	}

	session_destroy();

	header("Location: finish.php");
