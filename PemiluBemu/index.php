<?php
	//get voters
	require_once("database.php");
	$voters = see_voter(); //perform "SQL SELECT COUNT(done) FROM user"
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Login screen">
    <meta name="author" content="A.H.I.E">

	<title>Login | Pemilu BEMU UKDW 2016</title>

	<link rel="icon" href="img/ref-icon.png" type="image/png" sizes="16x16">

	<!-- Latest compiled and minified CSS -->
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<link href="css/loginStyle.css" rel="stylesheet">
	<link href="//maxcdn.bootstrapcdn.com/font-awesome/4.2.0/css/font-awesome.min.css" rel="stylesheet">
</head>

<body>
<div class="container">
    <div class="row">
        <div class="col-md-offset-5 col-md-4">
            <div class="form-login">
            <h4>Pemilu BEMU UKDW 2016</h4>
            <form method="post" action="login.php">
	            <input type="text" id="userName" name="nama" class="form-control input-sm chat-input" placeholder="Nama Lengkap" />
	            <br>
	            <input type="text" id="userPassword" name="nim" class="form-control input-sm chat-input" placeholder="NIM" />
	            <br>
	            <h5 style="text-align: center; color: #777;">Isi nama lengkap sesuai dengan nama lengkap di SSAT</h5>
	            <br>
	            <div class="wrapper">
		            <span class="group-btn">
		                <button class="btn btn-primary btn-md" type="submit">Login <i class="fa fa-sign-in"></i></button>
		            </span>
	            </div>
            </form>
            </div>
            <h5 style="text-align : center;">Jumlah suara masuk : <?php echo $voters ?></h5>
        </div>
    </div>
</div>
</body>
</html>