<head>

    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <meta name="description" content="Done vote">
    <meta name="author" content="A.H.I.E">

    <title>Pemilu BEMU 2016</title>

    <link rel="icon" href="img/ref-icon.png" type="image/png" sizes="16x16">

    <!-- Bootstrap Core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">

    <!-- Custom CSS -->
    <link href="css/stylish-portfolio.css" rel="stylesheet">

    <!-- Custom Fonts -->
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css">
    <link href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,700,300italic,400italic,700italic" rel="stylesheet" type="text/css">

</head>

<body>
    <div class="container">
        <!-- Modal Setuju-->
        <div class="modal-dialog">
            <!-- Modal content-->
            <form method="post" action="index.php">
                <div class="modal-content">
                <div class="modal-header" style="background-color:#5CB85C;">
                    <h4 class="modal-title" style="color:#FFF;">Informasi</h4>
                </div>
                <div class="modal-body">
                    <p>Terima kasih telah menggunakan hak pilih anda.</p>
                </div>
                <div class="modal-footer">
                    <input type="submit" class="btn btn-default" value="Keluar">
                </div>
                </div>
            </form>
        </div>
</body>

</html>